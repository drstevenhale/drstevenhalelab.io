---
layout: page
title: Publications
permalink: /publications/
---

ORCiD ID: [0000-0002-6402-8382](http://orcid.org/0000-0002-6402-8382)

Full publication list is available on [Google Scholar](https://scholar.google.co.uk/citations?hl=en&user=gY80OdQAAAAJ&view_op=list_works&sortby=pubdate).
