#
#  MAKEFILE
#
#    Steven Hale
#    2019 June 8
#    Birmingham, UK
#
# This is a Jekyll website.  To build it you need
#   # dnf install ruby ruby-devel @development-tools
#

################################################################

.PHONY: all dep build serve clean

all: build

################################################################

##
# Tell the user what the targets are.
##

help:
	@echo
	@echo "Targets:"
	@echo "   prerequisites    Install prerequisites."
	@echo "   dependencies     Install dependencies."
	@echo "   create           Create new site."
	@echo "   new              Create new with all requirements."
	@echo "   build            Build the static site."
	@echo "   serve            Serve the site from localhost:4000."
	@echo "   draft            Serve the site from localhost:4000 including _drafts."
	@echo "   clean            Remove all files and directories in .gitignore."
	@echo

# Run in --silent mode unless the user sets VERBOSE=1 on the
# command-line.

ifndef VERBOSE
.SILENT:
endif

prerequisites:
	gem install --user-install jekyll bundler http_parser.rb

dependencies:
	bundle config set --local path 'vendor/bundle'
	bundle install

create: prerequisites
	jekyll new --force --skip-bundle .

new: prerequisites create dependencies

build: dependencies
	JEKYLL_ENV=production \
	bundle exec jekyll build

serve: dependencies
	JEKYLL_ENV=production \
	bundle exec jekyll serve

draft: dependencies
	JEKYLL_ENV=production \
	bundle exec jekyll serve --draft

clean:
	git clean -dxf
