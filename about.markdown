---
layout: page
title: About
permalink: /about/
---

Steven Hale leads the operation and development of the international
Birmingham Solar Oscillations Network (BiSON), a global network of
automated robotic solar telescope run by the University of Birmingham
in the UK. His research interests are instrumentation and electronics,
and high-resolution optical spectroscopy techniques. In his spare time
he has many interests including photography and aviation, and has a
private helicopter license rated on the Robinson R22 and R44 aircraft.

This is a private blog and in no way represents opinions or
endorsements from the University of Birmingham.
