# GitLab Homepage

[![pipeline status](https://gitlab.com/drstevenhale/drstevenhale.gitlab.io/badges/dev/pipeline.svg)](https://gitlab.com/drstevenhale/drstevenhale.gitlab.io/commits/dev)

This website is available via [GitLab
Pages](https://drstevenhale.gitlab.io/) and my [personal
domain](https://www.stevenhale.co.uk/).
